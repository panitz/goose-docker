ARG GOLANG_VERSION=1.16.3-buster
ARG UBUNTU_VERSION=focal-20210416
FROM golang:${GOLANG_VERSION} AS builder

ARG GOOSE_VERSION
RUN go get -u github.com/pressly/goose/cmd/goose${GOOSE_VERSION}

FROM ubuntu:${UBUNTU_VERSION}
COPY --from=builder /go/bin/goose /usr/local/bin
WORKDIR /data
ENTRYPOINT [ "/usr/local/bin/goose" ]
CMD [ "status" ]
