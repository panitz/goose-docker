# Docker image with pressly/goose

This repository contains a docker image with the [goose database migration tool](https://github.com/pressly/goose).

## Building the docker image

```shell
docker build -t panitz/goose:latest .
```

The Dockerfile accepts the following build arguments:

| Build Argument | Default | Description |
|----------------|---------|-------------|
| GOOSE_VERSION | | Set the tag of pressly/goose to build. If you want to use this argument then the value has to start with "@"! |
| GOLANG_VERSION | 1.16.3-buster | Version of the golang build image. |
| UBUNTU_VERSION | focal-20210401 | Version of the Ubuntu base image. |

```shell
docker build --build-arg GOOSE_VERSION=@v2.7.0 -t panitz/goose:v2.7.0 .
```
